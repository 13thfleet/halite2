# Welcome to the main package. This is where the bot starts.
# Try to keep the code here minimal.
# This is the begin of the program. Call "python3 main.py" to start the bot.
import hlt
from src import bot
import sys


NAME = '13th Fleet'
#Initialize the bot.
bot = bot.Bot(NAME)
bot.argv= sys.argv
game = hlt.Game(NAME)
while True:
    bot.takeTurn(game.update_map())
    commands = bot.generateCommandQueue()
    if commands is not None:
        game.send_command_queue(commands)

