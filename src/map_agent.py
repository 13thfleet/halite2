from . import agent
from . import state

class gen_next_state:
    def gen_state_list():
        return list()
    def gen_states():
        """
            Should be implemented here.
            We should talk about 
            How we generate a list of states in general
            gen_state_list should evaluate them, and filter
        """
        return None

# Example Implemetations
class top(gen_next_state):
    def __init__(self,top=None):
        if top is None:
            self.top=3
        elif not isinstance(int, top):
            raise TypeError
        else:
            self.top = top
    def gen_states(self):
        l = gen_state_list
        if top < l.len():
            return l[1:top]

class top_frac(gen_next_state):
    def __init__(self,top=None):
        if top is None:
            self.top=0.2
        elif not isinstance(float, top):
            raise TypeError
        else:
            self.top = top
    def gen_states(self):
        l = gen_state_list
        if top >0 and top <1:
            return l[1:round(l.len()*top)]

class MapAgent:
    def __init__(self , my_id, width, height):
        pass
    
