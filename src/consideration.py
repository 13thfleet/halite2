import json 
from .decisionmaker import *
from .curves import *
from . import imap

class CurveFromFile(Curve):
    pass

class LogFromFile(CurveFromFile, Log):
    def __init__(self, Filename):
        with open(Filename, "r") as myfile:
            ob = json.load(myfile)
        super(Log,self).__init__(**ob)

class PolynFromFile(CurveFromFile, Polyn):
    def __init__(self, Filename):
        with open(Filename, "r") as myfile:
            ob = json.load(myfile)
        super(PolynFromFile, self).__init__(**ob)

class ExpFromFile(CurveFromFile, Exp):
    def __init__(self, Filename):
        with open(Filename, "r") as myfile:
            ob = json.load(myfile)
        super(ExpFromFile, self).__init__(**ob)

class RootFromFile(CurveFromFile, Root):
    def __init__(self, Filename):
        with open(Filename, "r") as myfile:
            ob = json.load(myfile)
        super(RootFromFile, self).__init__(**ob)

class ConsiderationfromString(consideration):
    def __init__(self, string):
        ob = json.loads(string)
        module = src.curves
        class_ = getattr(module, ob["response_curve"]["__type__"])
        response_curve = class_(ob["response_curve"])
        super().__init__(**ob)
        self.response_curve=response_curve

class ConsiderationfromFile(ConsiderationfromString):
    def __init__(self, Filename):
        with open(Filename, "r") as myfile:
            string = myfile.read()
        super().__init__(string)

class ImapConsideration(ConsiderationfromFile):
    def __init__(self, imap,  Filename="../Considerations/ImapConsideration.json", **kwargs):
        if not isinstance(imap, InfluenceMap):
            raise TypeError("Imap is of wrong Type")
        super().__init__(Filename)
        self.imap = imap
        sel.context.imap = imap
        context.imap = imap
    
    def score_def(context, bonus, minimum):
        pass

class Attack_ImapConsideration(ImapConsideration):
    def __init__(self, imap, Filename = "../Considerations/AttackImapConsideration.json", **kwargs):
        super().__init__(Filename)
