from . import state
from state import State 

class ErrorCodes(enum):
    success = auto()
    no_success = auto()
    partial_success = auto()
    partially_unsuccessful = auto()
    last_error = auto()

class Action(enum):
    ship_dodge = auto()
    ship_attack = auto()
    ship_suicide = auto()
    ship_dock = auto()
    ship_reproduce = auto()
    ship_follow = auto()
    map_rush = auto()
    map_lms = auto()
    map_expand = auto()
    map_defend_territory = auto()
    last = auto()

class computation:
    def compute_diff_hp(state, state_prev):
        if not isinstance(State, state):
            raise TypeError
        if not isinstance(State, state_prev):
            raise TypeError
        ship_list = state.get_own_ships()
        prev_ship_list = state_prev.get_own_ships()
        sum = 0
        prev_sum = 0
        for i in ship_list:
            sum += i.health

        for i in prev_ship_list:
            prev_sum += i.health
        return prev_sum - sum

    """
    Collisions means trying to do a suicide attack
    Implemetation looks like following,
    There exists a list, which saves the actual ship actions
    and targets should also be saved.
    Successful : Good Thrust
    Unsuccessful : Ship dodged and is alive
    Partially_Successful :  Enemy ship has died before, your's alive 
    Partially_Unsuccessful : Enemy Ship is alive but your's died 

    When the suicide Action is being set,
    a ship or ship_id must be specified and 
    the enemy ship must be set as n.target.

    This is important for analysis, since you can
    probably create a probability distribution, 
    for range, and health of the ships
    """

    def compute_successful_collisions(state, state_prev): 
        if not isinstance(State, state):
            raise TypeError
        if not isinstance(State, state_prev):
            raise TypeError
        
        prev_list_dic = state_prev.get_action_list()
        prev_list = prev_list_dic[Action.ship_suicide]
        new_list = state.get_own_ships()
        new_enemy_list = state.get_enemy_ships()

        valid_ship_list = [n for n in prev_list if n in new_list and not n.target in new_enemy_list ]
        return valid_ship_list

    def compute_unsuccessful_collisions(state, state_prev):
        if not isinstance(State, state):
            raise TypeError
        if not isinstance(State, state_prev):
            raise TypeError
        prev_list_dic = state_prev.get_action_list()
        prev_list = prev_list_dic[Action.ship_suicide]
        new_list = state.get_own_ships()
        new_enemy_list = state.get_enemy_ships()

        valid_ship_list = [n for n in prev_list if n in new_list and n.target in new_enemy_list ]
        return valid_ship_list

    def compute_partially_unsuccessful_collisions(state, state_prev):
        if not isinstance(State, state):
            raise TypeError
        if not isinstance(State, state_prev):
            raise TypeError
        prev_list_dic = state_prev.get_action_list()
        prev_list = prev_list_dic[Action.ship_suicide]
        new_list = state.get_own_ships()
        new_enemy_list = state.get_enemy_ships()

        valid_ship_list = [n for n in prev_list if not n in new_list and n.target in new_enemy_list ]
        return valid_ship_list

    def compute_partially_successful_collisions(state, state_prev):
        if not isinstance(State, state):
            raise TypeError
        if not isinstance(State, state_prev):
            raise TypeError
        prev_list_dic = state_prev.get_action_list()
        prev_list = prev_list_dic[Action.ship_suicide]
        new_list = state.get_own_ships()
        new_enemy_list = state.get_enemy_ships()

        valid_ship_list = [n for n in prev_list if n in new_list and not n.target in new_enemy_list and n.success]
        return valid_ship_list

    def compute_probability_of_action(state, ship_id):
        if not isinstance(State, state):
            raise TypeError
    def compute_probability_of_goal(state):
        if not isinstance(State, state):
            raise TypeError
    def compute_ships_in_range(state, ship_id):
        """
        Return Type:
        own_ships, enemy_ships
        """
        if not isinstance(State, state):
            raise TypeError
    def compute_probability_to_die(state, ship_id):
        if not isinstance(State, state):
            raise TypeError
