from entity import Ship as ship


def base_react():
    pass

def base_defense():
    pass

def base_chases():
    pass

class _base_ship(ship):
    # This Class is going the be filled with Parameters like Behaviour
    # Parameter : Normal_React(self,Group,Fleet,Mode,Target):str
    # Parameter : React_On_Chase(self,Group,Fleet,Mode,Target):str
    # Parameter : Group : List<entity.Ships>
    react=None
    chase_react=None
    defense_react=None
    Mode=None
    Group=None
    Fleet=None

    def __init__(self, Normal_React=None, React_On_Chase=None,React_on_Defense,React_Death_Thread, Mode=None, Group=None, Fleet=None, Defense_Mode=None, Target=None):
        self.react=Normal_React
        self.chase_react=React_On_Chase
        self.defense_react=React_on_Defense
        self.Mode=Mode
        self.Group=Group
        self.Fleet=Fleet

class Settler(ship):
