import hlt;

import logging;
import math;
import numpy as np;
from inspect import currentframe, getframeinfo

game = hlt.Game("13THFLEET")
logging.info("Opponent approaching")

#print currentlinenumber
#print (currentframe().f_lineno)
#you may not print since commands are communitated via stdio
logging.info(currentframe().f_lineno)


i=0
roles=[]
'''
Strategic role dictionary
None
1 meaning colonizing
2 meaning defending
3 meaning attack
4 meaning hiding in the corner
'''


#initializing things
def has3dockspots(self):
    return self.num_docking_spots ==3;

#def attack(self):
    #navigate to the closest planet
    #return;



def defend(self):
    planets = game_map.all_planets();
    count= 0
    vec =[0,0]
    logging.info(currentframe().f_lineno)
    for planet in planets:
         if planet.owner==game_map.get_me().id:
             count+=1
             vec[0]+=planet.x
             vec[1]+=planet.y

    logging.info(currentframe().f_lineno) #getting the middle point
    logging.info(str(count))
    for i in vec:
        logging.info(currentframe().f_lineno)
        if(count==0):
            count = 1
        i= int (i/count)
    logging.info(currentframe().f_lineno)
    mpoint= hlt.entity.Position(vec[0],vec[1])

    logging.info(currentframe().f_lineno)
    mdis = self.calculate_distance_between(mpoint)
    logging.info(currentframe().f_lineno)
    for foreign_entity in game_map._all_ships():
        logging.info(currentframe().f_lineno)
        if self.calculate_distance_between(foreign_entity) <= mdis:
            logging.info(currentframe().f_lineno)
            if foreign_entity.owner!=game_map.get_me():

                logging.info(currentframe().f_lineno) #getting the middle point

                navigate_command = self.navigate(self.closest_point_to(foreign_entity),game_map,speed=int(hlt.constants.MAX_SPEED),ignore_ships=False)
                self.entity_target=foreign_entity
                logging.info(currentframe().f_lineno) #getting the middle point
                if navigate_command:
                    logging.info(currentframe().f_lineno)
                    return navigate_command
                logging.info(currentframe().f_lineno)
    er = self.navigate(self.closest_point_to(mpoint),game_map,speed=int(hlt.constants.MAX_SPEED),ignore_ships=False);
    if er:
        return er
    return "";



def settle(self):
    unowned=game_map._all_unowned_planets();
    unowned.sort(key=self.calculate_distance_between)
    for planet in unowned: #if can settle, choose a planet
        navigate_command = self.navigate(
            self.closest_point_to(planet),
            game_map,
            speed=int(hlt.constants.MAX_SPEED*5/6),
            ignore_ships=False)

        if navigate_command:
            logging.info(currentframe().f_lineno)
            self.entity_target= planet;
            return navigate_command;
    logging.info("Nothing to capture, will defend")
    return ;   #swap to something else







while True:
    #turn start
    i+=1
    command_queue = []
    game_map = game.update_map()


    planets = game_map.all_planets()
    myships=game_map.get_me().all_ships()
    planets.sort(key=myships[0].calculate_distance_between)
    planets.sort(key=has3dockspots)
    targets=[]
    unowned=game_map._all_unowned_planets();
    targets.extend(unowned)
    for player in game_map.all_players():
        targets.extend(player.all_ships())

    #for planet in planets:
    #    logging.info(str(planet))
    if len(myships)<6:
        #early game
        #do the example code until you have x amount of ships

        for ship in myships:
            if ship.docking_status != ship.DockingStatus.UNDOCKED:
                continue
            unowned.sort(key=ship.calculate_distance_between)
            for planet in unowned:
                    #if planet.is_owned():
                    #    continue
                    if ship.can_dock(planet):
                        command_queue.append(ship.dock(planet))
                        logging.info(str(ship)+"docks at: " +str(planet))

                    else:
                        navigate_command = ship.navigate(
                            ship.closest_point_to(planet),
                            game_map,
                            speed=int(hlt.constants.MAX_SPEED*5/6),
                            ignore_ships=False)

                        if navigate_command:
                            command_queue.append(navigate_command)
                            logging.info(currentframe().f_lineno)
                            ship.entity_target= planet;
                    break
    else:
        logging.info(currentframe().f_lineno)
        j = 0
        for ship in myships:
            logging.info(currentframe().f_lineno)
            if ship.id not in roles:
                j=j+1
                j=j//2
                #print(str(j)+"\n") dont do this
                roles[ship.id]= j
            logging.info(currentframe().f_lineno)
            if(roles[ship.id]==1):
                logging.info(currentframe().f_lineno)
                command_queue.append(settle(ship));
            else:
                logging.info(currentframe().f_lineno)
                command_queue.append(defend(ship));
                #logging.info("Advising "+str(j)+  " to "+ str(ship.id))
                #logging.info(currentframe().f_lineno)





    game.send_command_queue(command_queue)
    logging.info("Turn "+str(i) +" over")












                                    #turn done
