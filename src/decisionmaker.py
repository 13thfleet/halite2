import src.curves 
import numpy
class static_calc():
    def calc_compensation_factor(num_Considerations, Score):
        ModificationFactor = 1- (1/ NumConsiderations)
        MakeUpValue = (1 - Score) * ModificationFactor
        return Score + (MakeUpValue * Score)

class consideration():
    def __init__(self, bonus, minimum, response_curve, **kwargs):
        self.bonus = bonus
        self.minimum = minimum
        self.response_curve = response_curve

    def score(self, context, bonus=None, minimum=None): # This is important
        if bonus is None:
            bonus=self.bonus
        if minimum is None:
            minimum = self.minimum
        return score_def(context, bonus, minimum)
    def score_def(context, bonus , minimum):
        string = "Implement this function in {}".format(consideration.__name__)
        raise NotImplementedError(string)
    # Define this function!
    def ComputeResponseCurve(self, score):
        return self.response_curve.value(score)

class decision_score_evaluator():
    def __init__(self, considerations, score):
        self.considerations = considerations

    def consider(self, context, bonus , minimum):
        final_score = bonus
        for consideration in considerations:
            if final_score < 0.0 or final_score < minimum:
                break
            score = consideration.score(context) 
            response = consideration.ComputeResponseCurve(score)
            final_score *= numpy.clip(response, 0, 1)
    
class Actions:
    def __init__():
        pass

class action_chain_def():
    def __init__(self):
        self.actions= list()
    def action_chain(self): 
        return self.actions

class Decisions:
    def __init__(self, action_chain_def, decision_score_evaluator, parameter = None):
        self.actions= action_chain_def.action_chain()
        self.decision_score_evaluator = list()
        for i in decision_score_evaluator:
            add_decision_evaluator(i)
    def add_decision_score_evaluator(self, evaluator):
        if i not in self.decision_score_evaluator:
            self.decision_score_evaluator.append(evaluator)
    def evaluate(self):
        score = 0
        for i in self.decision_evaluator:
            consideration = i.consider()
            compensated_factor = static_calc.calc_compensation_factor(self.decision_evaluator.len(),consideration)
            pass
        return score

class DecisionContext:
    def getbonusfactor(decision_context):
        pass
    def __init__(self,):
        evaluator = list()

class DecisionMaker:
    def __init__(self):
        pass
    def scoreDecisions(self, decisions, decision_context):
         cutoff = 0
         for i in decisions:
            bonus = i.context.getbonusfactor(decision_context)
            if bonus < cutoff:
                continue
            score =0
            for j in i.decision_evaluator:
                tmp = j.consider(context, bonus, cutoff)
                if score < tmp:
                    score = tmp
            i.score=score
            if decision.score > cutoff:
                cutoff = score

