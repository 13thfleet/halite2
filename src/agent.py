#from ../hlt import entity 
"""
Base class for the agent.
"""


class Agent :
    def __init__(self, behaviour , player):
       self.state=None
    def __update_state__(self):
        pass
    def update(self, map=None):
        if map == None:
            raise NotImplementedError('Map shouldn\'t be None')
        super().__update_state__(self, map)
class ShipAgent_Interface(Agent):
    def __init__(self, behaviour, player):
        super(ShipAgent_Interface, self).__init__()
class MapAgent_Interface(Agent):
    def __init__(self, behaviour , player, ships = list()):
        super(MapAgent_Interface, self).__init__(self, behaviour, player)
        if not isinstance(list, ships):
            raise TypeError
        self.ships=ships
    def add_ship_agent(self, ship=None):
        if not ship:
            return 
        if not isinstance(entity.Ship):
            raise TypeError
        pass
    def remove_ship_agent(self, key=None):
        if key == None:
            return
        if isinstance(int, key):
            if key >0 and key < self.ships.len():
                self.ships.pop(key)
        else:
            raise TypeError
