import math;
import numpy;
import hlt;


def makegraph(ships):

    graph={}
    for ship in ships:
        graph[ship]=[]
        for target in game_map._all_ships():
            if target.owner=game_map.get_me().id :
                continue
            elif ship.calculate_distance_between(target)>=7:
                continue
            dis = ship.calculate_distance_between(target)
            distances= gamen_map.nearby_entities_by_distance(target)
            count = 1
            for entity,distance in distances.sort(distances.item,key=lambda item: item[1]):
                if entity.radius>hlt.constants.SHIP_RADIUS: #nota ship contiue
                    continue
                if distance <=dis:
                    count+=1
                else:
                    break
            #here some room for changes to the flow
            flow = hlt.constants.WEAPON_DAMAGE

            graph[ship].append((target, (int)flow/count))
    return graph #graph as a adjancency dictionary

def bfsearch(graph):
    vis=[]
    par={}
    queue=[]
    for node in graph:
        queue.append(node)
    while len(queue)>0:
        for point in queue[0]:
            queue.append(point)
