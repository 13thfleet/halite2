#!/usr/bin/env python
# -*- coding: utf-8 -*-

prediction_ship_position = {}

class prediction():
    def __init__(self, function, empty_state):
        self.func = function
        self.state = empty_state
        self.timestamp = 0
    def update(self,map):
        self.state = self.func(self.state, map)
        self.timestamp= map.timestamp
    def get_prediction_update(self,map):
        self.update(map)
        return self.state
    def get_prediction():
        return self.state

class ship_position_prediction(prediction):
    def __ship_position__(self ,state,map):
        a =  { "registered_ships" : {},  "position": {}, "vector" : {}, "prediction": {}} 
        ships = [ n for i in map.all_players() if i != map.get_me()  for n in i.all_ships() ]
        for i in ships:
            a["registered_ships"][i.id] = True 
            a["position"][i.id] = (i.x,i.y)
            if state[map.timestamp-1] is not None:
                if state[map.timestamp-1]["registered_ships"] is not None:
                    if state[map.timestamp-1]["registered_ships"].get(i.id) is None:
                        continue
            pos = state[map.timestamp-1]["position"][i.id]
            vec_1 =( i.x -pos[0], i.y - pos[1])
            a["vector"][i.id] = vec_1
            if state[map.timestamp-1]["vector"].get(i.id): 
                vec_2 = state[map.timestamp-1]["vector"][i.id]
                a["prediction"][i.id] = ((vec_1[0] + vec_2[0])/2, (vec_1[1] - vec_2[1])/2)
                # Here could be something smarter.
        state[map.timestamp] = a
        return state
    def __init__(self):
        a= lambda x,y : self.__ship_position__(x,y)
        b = { 0 : { "registered_ships" : {},  "position": {}, "vector" : {}, "prediction": {}} }
        super().__init__(a, b)


class prediction_manager():
    def __init__(self,list_of_predictions =dict()):
        self.predictions = list_of_predictions
        self.add_prediction("ship_position", ship_position_prediction())
    def add_prediction(self,name, prediction):
        self.predictions[name] = prediction
    def remove_prediction(self, name):
        self.predictions.pop(name,None)
    def update(self, name, map, to_return=True):
        if self.predictions[name] is not None and self.predictions[name].timestamp < map.timestamp :
            a= self.predictions[name].get_prediction_update(map)
        elif self.predictions[name] is not None: 
            a = self.predictions[name].get_prediction()
        else:
            raise Exception("Prediction not given.")
        if to_return:
            return a 
    def update_all(self, map):
        for key,value in self.predictions:
            update(key,map , to_return=False)

