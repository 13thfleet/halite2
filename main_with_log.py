# Welcome to the main package. This is where the bot starts.
# Try to keep the code here minimal.
# This is the "start" of the program. Call "python3 main.py" to start the bot.
import hlt
from src import bot
import sys


NAME = '13th Fleet'
#Initialize the bot.
bot = bot.Bot(NAME)
bot.argv= sys.argv
game = hlt.Game(NAME)
while True:
    bot.takeTurn(game.update_map())
    commands = bot.generateCommandQueue()
    f = open("log","at")
    for i in commands:
        if i is not None:
            f.write(str(i) )
    f.write("\n")
    f.close()
    if commands is not None:
        game.send_command_queue(commands)
