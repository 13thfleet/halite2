#!/usr/bin/env python
# -*- coding: utf-8 -*-
import math

class Line():
    def __init__(self, a, b,c , **kwargs):
        self.a = a
        self.b = b
        self.c = c
    def Schnittpunkt_Gerade_Kreis(self, x,y ,radius, **kwargs):
        d = c - self.a*x - self.b *y
        if radius**2 * (self.a **2 + self.b**2) == d **2:
            x1 = x + ( a*d + b * math.sqrt(radius**2 * (a**2 + b**2) - d**2)) / (a**2 + b**2)
            y1 = y + ( b*d - a * math.sqrt(radius**2 * (a**2 + b**2) - d**2)) / (a**2 + b**2)
            return (x1, y1)
        if radius**2 * (self.a **2 + self.b**2) > d **2:
            x1 = x + ( a*d + b * math.sqrt(radius**2 * (a**2 + b**2) - d**2)) / (a**2 + b**2)
            x2 = x + ( a*d - b * math.sqrt(radius**2 * (a**2 + b**2) - d**2)) / (a**2 + b**2)
            y1 = y + ( b*d - a * math.sqrt(radius**2 * (a**2 + b**2) - d**2)) / (a**2 + b**2)
            y2 = y + ( b*d + a * math.sqrt(radius**2 * (a**2 + b**2) - d**2)) / (a**2 + b**2)
            return [(x1,y1), (x2,y2)]

    @staticmethod
    def calculate_line(x,y , degree):
        m = math.tan . math.degree(degree)
        n = y - m * x
        return Line(-m,1, n )

class Circle():
    def __init__(self,x,y,radius,**kwargs):
        self.x=x
        self.y=y
        self.radius=radius

    def Intersect2Circles(A,a,B,b):
        AB0 = B[0] - A[0]
        AB1 = B[1] - A[1]
        c = math.sqrt(AB0 ** 2 + AB**2)
        if c == 0 :
            return None
        x = (a**2 + c**2 - b**2) / (2*c)
        y = a**2 - x**2
        if y < 0 :
            return None
        y = math.sqrt(y)
        ex0 = AB0 / c
        ex1 = AB1 /c
        ey0 = -ex1
        ey1 = ex0
        Q1x = A[0] + x * ex0
        Q1y = A[1] + x * ex1
        Q2x = Q1x - y * ey0
        Q2y = Q1y - y * ey1
        Q1x += y * ey0
        Q2x += y * ey1
        return [(Q1x, Q1y), (Q2x,Q2y)]


    def schnittpunkte(self,x,y,radius,**kwargs):

        if (self.radius + radius)**2 > (self.x -x)**2 + (self.y -y)**y :
            return None
        elif (self.radius + radius)**2 == (self.x -x)**2 + (self.y -y)**y :
            # One Point Overlap
            vec = (self.x -x , self.y-y) * self.radius
            return (self.x ,self.y ) + vec
        else:
            return Intersect2Circles((self.x,x), self.radius , (x,y), radius)

    def calculate_all_degrees_between_two_points(point,target_1, target_2):
        bound_1 = math.floor(math.degree(math.atan2(target_1[1] - point[1], target_1[0]- point[1])))
        bound_2 = math.ceil(math.degree(math.atan2(target_2[1] - point[1], target_2[0]- point[1])))
        if bound_2 < bound_1:
            return range(bound_1, 359, 1) + range(0,bound_2,1)
        return range(bound_1, bound_2, 1)
     


class Polynom():
    def __init__(self, list_in, list_out, **kwargs): 
        self.in_list= list_in
        self.out_list= list_out

    def is_in(self, x,y , **kwargs):
        for i in self.list_in:
            if (i.x-x)**2 + (i.y-y)**2 > i.radius**2: 
                return False
        for i in self.list_out:
            if (i.x-x)**2 + (i.y-y)**2 < i.radius**2: 
                return False
        return True
