from src.predictions import *
from src.decisionmaker import DecisionMaker
from src.decisionmaker import Decisions
from src.decisionmaker import consideration
from src.decisionmaker import action_chain_def
from src import consideration
from src.consideration import ConsiderationfromFile
from src import curves
from src.imap import InfluenceMap
import math
from hlt import entity
from hlt import constants
from constants import * ;

# Example Consideration
# Attack if an enemy is nearer .

class AttackImapConsideration(ConsiderationfromFile):
    def __init__(self, map_v, Filename="Considerations/AttackImapConsideration.json" ):
        self.imap = InfluenceMap(map_v, generate=True)
        super().__init__(Filename)

    def score_def(context, bonus, minimum ):
        # The map is the context.
        self.imap.regenerateMap(context.map)
        ship = context.ship
        max_v = 0 
        for i in imap.standard_range(ship.x,ship.y):
            if self.imap.cells[i[0]][i[1]] > max_v:
                max_v = self.imap.cells[i[0]][i[1]]
        if max_v > minimum :
            return max_v + minimum
        else:
            return 0


class Bot:
    def __init__(self, name):
        self.timestamp = 0
        self.prediction_v = ship_position_prediction()
        self.decisionMaker = DecisionMaker()
        self.action_chain = action_chain_def()
        self.settle_list = dict()
#        ConsiderationsToLoad = [
#            "AttackImapConsideration",
#            "Consideration_Linear",
#            "Consideration_Linear_2",
#            "Consideration_Weird_Exponential_Curve",
#            "Consideration_Weird_Logarithmic_Curve",
#        ]
#       ConsiderationsToLoad = map(lambda x: "Considerations/"+x+".json", ConsiderationsToLoad )
#       self.Considerations = map(lambda x:  ConsiderationsFromFile(x), ConsiderationsToLoad)
#       action_score_considerations.append(consideration(0,0,self.Considerations[0]))
        self.action_score_considerations = []
        #self.action_decision = Decisions(action_chain_def=None, decision_score_evaluator )
        #self.action_decision.
        return

    def in_list (a, in_list):
        if a in in_list:
            return True
        else:
            return False


        
    def makePlan(self, own, enemy_ships):
        """
        This function ought to optimize damage done to the enemy ships.
        Since damage is always split anyways it makes sense
        to reduce the overhead of damage done to ships,
        that should be destroyed.

        I describe the following algorithm.
        Incrementally own ships are filtered  by the number 
        of ships they surround. 
        The algorithm ought to surround the enemys
        with the ships given and test the current greedy
        next best solution.
        So no backtracking is applied.
        For Case 1 ships are just placed.
        For the all further cases it is tested if the 

        The return type is a List of triples, which
        hold the ship object / reference to it, the point
        and the List of enemy ships it attacks.

        To test how many enemys can be attacked I describe
        following algorithm: 
            For two circles: 
                range must be less 
            For more than two circles:
                Compute intersection/ middle point of two
                circles: and add try to add the nearest ship.
        """
        plan = list()
        counter = 0
        while not len(own)> 0 :
            counter += 1
            own_ships = [ i for i in own if len(enemy_ships[i]) == counter]
            if counter == 1 :
                for i in own_ships:
                    own.remove(i)
                    plan.append([i,None, enemy_ships[own]])
            else : 
                max_ships_destroyed = 0 # This values is priotized
                max_damage_done = 0
                for t in own_ships:
                    max_list = list()
                    max_point = None
                    intersection_point = None
                    for i in range(0, len(enemy_ships[t]) -1, 1):
                        for j in range(i, len(enemy_ships[t]) -1, 1):
                            if enemy_ships[t][i].calculate_distance_between(enemy_ships[t][j]) > 2* WEAPON_RADIUS: # Distance is less than two times the attack range
                                list_of_points = [enemy_ships[t][i], enemy_ships[t][j]]
                                # Compute the intersection of two points
                                x_calc = lambda x : map (lambda x: x.x ,x)
                                # Calls .x for every element and return s the list
                                y_calc = lambda y : map (lambda y: y.y ,y)
                                # Calls .y for every element
                                def point_list (point_list):
                                    return entity.Position(math.fsum (x_calc(point_list))/ len(point_list) , math.fsum (y_calc(point_list)) / len(point_list))
                                # math.fsum, summed up 
                                intersection_point = point_list(list_of_points)
                                points_set = [n for n in enemy_ships[t] if not n in list_of_points]
                                points_set.sort(key=lambda x : intersection_point.calculate_distance_between(x))
                                while len(points_set) > 0 and  intersection_point.calculate_distance_between(points_set[0]) <= constants.WEAPON_RADIUS:
                                    if point_set[0].hp is not None and point_set[0].hp > 0 :
                                        list_of_points.append(points_set[0]) # Append
                                        points_set.pop(0) # Filter all points
                                        intersection_point = point_list(list_of_points) # Recalculate middle position
                                        points_set.sort(key=lambda x : intersection_point.calculate_distance_between(x)) # resort the list
                                    else:
                                        points_set.pop(0)
                            if len(max_list) < len(enemy_ships[t]) and len(max_list) < len(points_set):
                                max_list = points_set # Update current point_set max_point = intersection_point # Update current 
                    if max_point is not None:
                        for i in max_list:
                            if i.hp is not None:
                                i.hp = i.hp - len(max_list)
                            else:
                                i.hp = i.health - len(max_list) # First occurence
                        plan.append([t,max_point, max_list])
        return plan

    def takeTurn(self, map):
        command_queue = list()
        self.timestamp += 1
        map.timestamp = self.timestamp  
        state = self.prediction_v.get_prediction_update(map)
        # print(state)
        state = state[map.timestamp]
        decision = "action" 
        #Seems like I don't have anything else implemented yet.
        settle_list = self.settle_list
        enemy_ships = dict()
        self.action_score_considerations.append(AttackImapConsideration(map, Filename="Considerations/AttackImapConsideration.json"))
        for i in state["registered_ships"]:
            pos = state["prediction"].get(i)
            if not pos: 
                vec = state["vector"].get(i)
                if vec is not None:
                    pos = state["position"][i]
                    pos = (vec[0] + pos[0], vec[1] + pos[1])
                    pos = entity.Position(pos[0], pos[1])
            enemy_ships[i] = pos
        if decision == "action":
            seed_planet = list()
            ship_dict = dict() # Dictionary of ships being in range
            for i in map.get_me().all_ships():
                ships_in_range = [j for n in map.all_players() if n is not map.get_me() for j in n.all_ships() if j.calculate_distance_between(i) < constants.MAX_SPEED + constants.WEAPON_RADIUS  ]
                ships_in_range = set(ships_in_range)
                if len(ships_in_range) > 0:
                    ship_dict[i] = ships_in_range
                else: #If no ship is in range dock at the nearest planet.
                    planets = map.all_planets()
                    multiplier = lambda planet : 1 if planet not in settle_list else int (len(settle_list.get(planet)) > 2)
                    # Prioritize planets with low population and prefer unpopulated ones, to populated ones 
                    # If all plantes are populated prefer those with higher docking places. 
                    planets.sort(key = lambda x:  i.calculate_distance_between(x)*multiplier(i) + int( x.owner is not map.get_me()) + x.num_docking_spots if  multiplier(i)== 0 else 0 )
                    for j in planets:
                        if j.health > 0 :
                            if not j.is_owned() or j.owner == map.get_me():
                                if not j.is_full():
                                    if not j in settle_list:
                                        settle_list[j.id] = []
                                    settle_list[j.id].append(i.id)
                                    if i.can_dock(j):
                                        command_queue.append(i.dock(j))
                                    else :
                                        command_queue.append(i.navigate(j, map, constants.MAX_SPEED))
                                    break;
            self.settle_list = settle_list
            A = self.makePlan(map.get_me().all_ships(), ships_in_range)
            for i in A:
                if i[1] is not None:
                    intersection_point = i[1]
                    #command_queue.append(
                    command = i[0].navigate(intersection_point, map, constants.MAX_SPEED, ignore_planets=False )
                    command_queue.append(command)
                else:
                    command = i[0].navigate([2], map, constants.MAX_SPEED, ignore_planets=False, ignore_ships=True)
                    command_queue.append(command)
        self.command_queue = command_queue
        return command_queue

    def generateCommandQueue(self):
        return self.command_queue
