import operator
import math;
import numpy as np;
import matplotlib.pyplot as plt;
from threading import Thread
import multiprocessing

class Curve(object):
    """docstring for ."""

    def __init__(self,x_growth_multiplier=1,x_growth_offset=0,y_growth_offset=0, y_growth_multiplier=1, **kwargs):
        self.x_growth_multiplier = x_growth_multiplier
        self.x_growth_offset = x_growth_offset
        self.y_growth_multiplier = y_growth_multiplier
        self.y_growth_offset = y_growth_offset
    def eval_x(self,x):
        return self.x_growth_multiplier * x + self.x_growth_offset    
    def eval_y(self, y):
        return self.y_growth_multiplier * y + self.y_growth_offset    
    def value(self,x):
        x = self.eval_y(self.eval_x(x))
        return clamp(x ,0,1)
    def print(self):
        x = np.linspace(0.001,1,999)
        y1= list(map(self.value,x))
        #print(y1)
        #plt.plot(x,y1)
        #plt.show()
        q = multiprocessing.Queue()
        simulate=multiprocessing.Process(None,forThread,args=(x,y1,))
        simulate.start()
        print("something")

        #t = Thread(target=forThread, args=(x,y1,))
        #t.start()

def forThread(x,y1):
    plt.plot(x,y1)
    plt.show()


def clamp(num, min_value, max_value):
   return max(min(num, max_value), min_value)

class Log(Curve):
    """docstring forlog"""

    def __init__(self,base=2, **kwargs):
        super(Curve).__init__(**kwargs)
        self.base = base
    def value(self,x):
        x = self.eval_x(x)
        if self.base == 10:
            y = math.log10(x)
        y = math.log(x,self.base)
        y = self.eval_y(y)
        y = clamp(y,0.000000001,1)

class Polyn(Curve):
    def __init__(self,liste, **kwargs):
        super(Curve).__init__(**kwargs) #in Polyn the end of the list is the offset
        self.list=liste #Polynomial function as list
        self.offset= 0

    def value(self,x):
        x = self.eval_x(x)
        temp = 0
        count= len(self.list)
        for i in self.list:
            count-=1
            temp += i*(x**count)
        y= temp
        return clamp(self.eval_y(y),0,1)
class Exp(Curve):
    def __init__(self,exp=2, **kwargs):
        super(Curve).__init__(**kwargs)
        self.exp=exp
    def value(self,x):
        x = self.eval_x(x)
        y = math.pow(x,self.exp)
        y = self.eval_y(y)
        return clamp(math.pow(y,self.exp),0,1)

class Root(Curve):
    def __init__(self,exp=2, **kwargs):
        super(Curve).__init__(**kwargs)
        self.exp=exp
    def value(self,x):
        x = self.eval_x(x)
        if 2==self.exp:
            y = math.sqrt(x)
        y = math.pow(x,1/self.exp)
        y = self.eval_y(y)
        return clamp(y,0,1)

class ComponentCurve(Curve):
    def carry_over(x,y):
        y.value(x.value())
    ops = {
        '+' : operator.add,
        '-' : operator.sub,
        '*' : operator.mul,
        '/' : operator.truediv,
        '%' : operator.mod,
        '^' : operator.xor,
        '**' : operator.pow,
    }
    def __init__ (self,operator,left,right,  **kwargs):
            super().init(**kwargs)
            module = __import__(__name__)
            class_ = getattr(module, left["__type__"])
            self.left= class_(left)
            class_ = getattr(module, right["__type__"])
            self.right= class_(right)
            self.operator=operator
    def value(self,x):
        if self.operator == 'carry_over' :
            return self.left.value(self.right.value(x))
        else:
            ops[self.operator](self.left.value(x), self.right.value(x))

