import math

#Needs adjusting
DEFAULT_RADIUS = 2
MAP_GRANULARITY = 7 

class InfluenceMap:
    def dist(x, y , x_2, y_2 ):
        return math.sqrt( (x-x_2)**2 + (y-y_2)**2)
    def linear_func( dist):
        return max(0,1 - (1/DEFAULT_RADIUS) * dist)
    def standard_iter (x):
       return iter ([  (j.x, j.y, j) for n in x.all_players() if n is not x.get_me() for j in n.all_ships() ]) 
    def standard_func (pos, x_i, y_i):
       return InfluenceMap.linear_func(math.sqrt((x_i - pos[0] ) **2 + (y_i - pos[1]) **2 ))
    def standard_range(self,x,y):
        return [(x_i,y_i) for x_i in range(int(max(x - DEFAULT_RADIUS,0)),int(min(x + DEFAULT_RADIUS , self.x_bounds ))) for y_i in  range(int (max(y - DEFAULT_RADIUS,0)),int(min(y + DEFAULT_RADIUS , self.y_bounds )))]

    def __init__(self, map, generate= False, genmapfunc= None):
        self.cells = None
        self.x_bounds = map.width // MAP_GRANULARITY
        self.y_bounds = map.height // MAP_GRANULARITY
        if generate:
            if genmapfunc != None:
                genmapfunc(self)
            else:
                self.standardMap()
        self.regenerateMap(map)
    
    def standardMap(self):
        iterator = lambda x : InfluenceMap.standard_iter(x)
        # Iterator with tripel, x position, y position and other variable
        func = lambda pos, x_i, y_i : InfluenceMap.standard_func(pos,x_i, y_i)
        range_func = lambda x,y : self.standard_range(x,y)
        if func != None:
            self.func = func
        else:
            func = self.func
        if iterator != None:
            self.iterator = iterator
        else:
            iterator = self.iterator
        if range_func != None:
            self.range_func = range_func
        else:
            range_func = self.range_func
        self.range_func = range_func
        self.func = func
        self.iterator = iterator

    def regenerateMap(self, map, func= None, iterator=None, range_func=None):
        #Init Array, old one gets eaten by GC
        #Iterator is of type lamba x:map -> iter()
        #func is of type lambda x , array -> array
        #range_func is of type x,y -> (iter ->  [x,y] )
        if func != None:
            self.func = func
        else:
            func = self.func
        if iterator != None:
            self.iterator = iterator
        else:
            iterator = self.iterator
        if range_func != None:
            self.range_func = range_func
        else:
            range_func = self.range_func
        self.cells = list()
        for row in range(self.x_bounds): 
            self.cells.append([0.0]*self.y_bounds) # Creating the Map
        it = iterator(map)
        for i in it:
            self.addToMap(i , func, range_func)
    def addToMap(self, ship, func, range_func):
        ship_x = ship[0] // MAP_GRANULARITY
        ship_y = ship[1] // MAP_GRANULARITY 
        for range_iter in range_func(ship_x,ship_y):
                self.cells[range_iter[0]][range_iter[1]] += func(range_iter, ship_x, ship_y )
    def printMap(self):
        for x in self.cells:
            for y in x:
                print(str(round(y,1)),end= ' ')
            print("")

class EnemyHealthInfluenceMap(InfluenceMap):
    def health_func(x,ship):
        return ship.health 
    def standardMap(self):
        super().standardMap()
        # self.iterator = lambda x : standard_iter(x) # Don't overwrite 
        # self.range_func = lambda x,y : standard_range(x,y) # Don't overwrite
        self.func = lambda x,y : EnemyHealthInfluenceMap.health_func(x,y)
    def addToMap(self, ship , func, range_func):
        for range_iter in range_func(x,y):
            self.cells[range_iter[0]][range_iter[1]] += func(range_iter, ship)
class HealthInfluenceMap(EnemyHealthInfluenceMap):
    def standard_iter(x):
        return iter(x.get_me().all_ships())
    def standardMap(self):
        super().standardMap()
        self.iterator = lambda x : HealthInfluenceMap.standard_iter(x) # Don't overwrite 

class AttackInfluenceMap(InfluenceMap):
    # Returns how many ships you can attack from a position 
    def attack_func(x,ship):
        return 1
    def attack_iter(x):
        return [j for i in InfluenceMap.standard_iter(x) for j in InfluenceMap.standard_range(j[0] // MAP_GRANULARITY, j[1] // MAP_GRANULARITY)]
    #   return all fields nearby the to enemies 
    def attack_range_iter(map,x,y):
    #   return all enemy ships nearby
        return [ n for n in InfluenceMap.standard_iter(map) and dist(n[0], n[1], x, y) < WEAPON_RADIUS  ]
    def standardMap(self):
        super().standardMap()
        self.iterator = lambda x : AttackInfluenceMap.attack_iter(x)
        self.range_func = lambda x,y : AttackInfluenceMap.attack_range_iter(map, x,y)
        self.func = lambda x,y : AttackInfluenceMap.attack_func(x,y)
    def regenerateMap(self, map):
        self.range_func = lambda x,y : AttackInfluenceMap.attack_range_iter(map, x,y)
        # Update the RangeFunc and proceed with normal procedure
        super().regenerateMap(map)
    def addToMap(self, ship , func, range_func):
        for range_iter in range_func(ship[0],ship[1]):
            self.cells[range_iter[0]][range_iter[1]] += func(range_iter, ship)
class EnemyAttackInfluenceMap(AttackInfluenceMap):
    # Instead of List of Enemy Ships, we just need our own.
    # And the functionality can basically stay the same.
    def standard_iter(x):
        return iter (x.get_me().all_ships)
    def standardMap(self):
        super().standardMap()
        self.iterator = lambda x : EnemyAttackInfluenceMap.standard_iter(x)
        
class AttackWillKill(AttackInfluenceMap):
    def addToMap(self, ship , func, range_func):
        tmp = [[0.0]*self.y_bounds ]*self.x_bounds
        for range_iter in range_func(ship[0],ship[1]):
            tmp[range_iter[0]][range_iter[1]] += func[0](range_iter, ship, self.cells[range_iter[0]][range_iter[1]])
        for range_iter in range_func(ship[0],ship[1]):
            self.cells[range_iter[0]][range_iter[1]] += func[1](range_iter, ship, tmp[range_iter[0]][range_iter[1]])
    def attack_func_1(x,ship, value):
        return 1
    def attack_func_2(x,ship, value):
        if ship[2].health < WEAPON_DAMAGE // value :
            return 1
        else:
            return 0
    def standardMap(self):
        super().standardMap()
        self.func = (lambda x,y,z :  AttackWillKill.attack_func_1(x,y,z), lambda x,y,z: AttackWillKill.attack_func_2(x,y,z) )
    
class EnemyAttackWillKill(AttackWillKill):
    def standard_iter(x):
        return iter(x.get_me().all_ships())
    def standardMap(self):
        super().standardMap()
        self.iterator = lambda x : EnemyAttackWillKill.standard_iter(x)

class ConcreteDamage(InfluenceMap):
    # A list of Integer values must be generated, for each field
    # Each Integer represents the number of friendly ships in fire range of an enemy ship in range.
    # The Range Iterator should be the range an enemy ship can fire.
    # The func value, then should be the number of ships, which do not belong to that enemy
    def __init__(self, map, Player= None, generate= False, genmapfunc= None):
        super().__init__(map, generate, genmapfunc)
        if not Player:
            Player = map.get_me()
        self.Player=Player

    def ConcreteDamage_Iter(x):
        return InfluenceMap.standard_iter(x) # Ships that are not ours
    def ConcreteDamageRange_Iter(x,y):
        return [(x_i , y_i ) for x_i in range (max(x- WEAPON_RADIUS) //MAP_GRANULARITY, min(x + WEAPON_RADIUS) // MAP_GRANULARITY) for y_i in  range (max(y- WEAPON_RADIUS)// MAP_GRANULARITY, min(y + WEAPON_RADIUS) // MAP_GRANULARITY) ]
    def ConcreteDamageFunc(self, map, ship):
        return WEAPON_DAMAGE / (len([n for player in map.all_players() and player != self.Player for n in player.all_ships() and dist(ship[0], ship[1], n.x,n.y ) > WEAPON_RADIUS]) +1)

        
    def standardMap(self):
        super().standardMap()
        self.iterator = lambda x : ConcreteDamage.ConcreteDamage_Iter(x)
        self.range_func = lambda x,y : ConcreteDamage.ConcreteDamageRange_Iter(x,y)
    def regenerateMap(self,map):
        self.func = lambda x: self.ConcreteDamageFunc(map, x)
        super().regenerateMap(map)
