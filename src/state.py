import copy
class state_enum(Enum):
    ownships = auto()
    enemyships = auto()
    prev_hp_sum = auto()
    prev_hp_list = auto()
    ship_action_list = auto()
    ship_previous_action = auto()
    enemy_ship_action_list = auto()
    # This is purely based on how humans would think.
    # Prolly enemy actions should just docking and movement and attack
    # since we don't know how the enemy would think.
    hp_sum = auto()
    hp_list = auto()
    last = auto()


class State:
# state_interface 
# parameters 
# dictionary of values
    #Static dictionary from enums to strings. Used for logging.
    name_dict= {
            state_enum.ownships:"own_ships",
            state_enum.enemyships:"enemy_ships",
            state_enum.prev_hp_sum:"prev_hp_sum",
            state_enum.prev_hp_list:"prev_hp_list",
            state_enum.hp_sum:"hp_sum",
            state_enum.hp_list:"hp_list",
            state_enum.ship_action_list:"ship_action_list",
            state_enum.enemy_ship_action_list:"enemy_ship_action_list"
        }
    def __init__(self, dict_=None):
        if isinstance(dict,dict_):
            self.__statedict__ = copy.deepcopy(dict_)
        else:
            self.__statedict__ = dict()
        self.__name_list__ = fill_list()
        self.to_update = dict()
        self.commit_series = list()
    def __query__(self, a=None):
        self.__statedict__[a] #Just let it throw the normal error.

    def update(self, param=None , value = None):
        if value is None:
            raise TypeError
        if param is None or not isinstance(str, param):
            raise TypeError
        self.to_update[param]= value

    def commit(self):
        for i in self.to_update:
            tmp = __statedict__[i]
            __statedict__[i] = to_update[i]
            to_update[i] = tmp
        self.commit_series.append(self.to_update)
        self.to_update = dict()

    def rollback(self):
        if self.commit_series.len() >0:
            l = commit_series.pop()
            for i in l:
                __statedict__[i] = l[i]
            self.to_update = dict()
        else:
            raise Error("Rollback before Commit!")

    def get_own_ships(self):
        return self.__query__(state_enum.ownships)
    def get_enemy_ships(self):
        return self.__query__(state_enum.enemyships)
    def get_prev_hp(self):
        return self.__query__(state_enum.prev_hp_sum)
    def get_prev_hp_list(self):
        return self.__query__(state_enum.prev_hp_list)
    def get_hp(self):
        return self.__query__(state_enum.hp_sum)
    def get_hp_list(self):
        return self.__query__(state_enum.hp_list)
    def get_action_list(self):
        return self.__query__(state_enum.ship_action_list)
    def get_enemy_action_list(self):
        return self.__query__(state_enum.enemy_ship_action_list)
